mkdocs==1.2.3
mkdocs-material==7.3.3
mkdocs-git-revision-date-localized-plugin==0.10.0
mkdocs-video==1.1.0