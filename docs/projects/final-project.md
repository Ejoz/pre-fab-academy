---
title: Final Project
---

# How to prepare for the Final Project


## November 18th
I start to slightly panicking about the final project. Jonah advised me to make some mechanisms to test some concepts before thinking of the big picture.

I could make a little circular platform with one or two mechanisms for a start.

## November 25th
The final project is more precise now. I will make my own little theater about [*R.U.R.*](https://en.wikipedia.org/wiki/R.U.R.), a play written by [Karel Čapek](https://en.wikipedia.org/wiki/Karel_%C4%8Capek) in 1920. This play introduced the word *robot*, based on *robota* which means *hard work* in Czech.

Based on this idea, I will propose a little scenery around robotised figures, programmable lights and apparent mechanisms.

## Planning
The key to succeed in the preparation of the final project is to plan ahead with the themed weeks of the FabAcademy. With this in mind, here is the plan.

|                                                      | Final Project                                                            |
| ---------------------------------------------------- | ------------------------------------------------------------------------ |
| Principles and Practices                             | Plan and sketch                                                          |
| Project Management                                   |                                                                          |
| Computer-Aided Design                                | Model the theater                                                        |
| Computer-Controlled Cutting                          | Cut the external parts of the theater                                    |
| Electronics Production                               | Animate a little scenery on the theater                                  |
| 3D Scanning and Printing                             | Print a character of the play (robot)                                    |
| Electronics Design                                   | Design a PCB to animate the scenery (or the lights)                      |
| Computer-Controlled Machining                        | Make a box / a library for the theater                                   |
| Embedded Programming                                 | Program the leds or scenery animation                                    |
| Mechanical Design, Machine Design                    |                                                                          |
| Input Devices                                        | Add buttons to control the theater (play, with or without sound...)      |
| Molding and Casting                                  |                                                                          |
| Output Devices                                       | Add sound devices to the theater                                         |
| Networking and Communications                        | Connect everythoing together (scenery, lights and music)                 |
| Interface and Application Programming                | Create a application that can remotely command the beginning of the play |
| Wildcard Week                                        |                                                                          |
| Applications and Implications                        |                                                                          |
| Invention, Intellectual Property and Business Models |                                                                          |
| Project Development                                  | Put everything together                                                  |
| Final Project Requirements                           |                                                                          |

