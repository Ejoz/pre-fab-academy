---
title: TacTik
---

# Board Game and CNC

I would like to test the ShopBot this week, and for that I wanted a small and funny project. Fortunately a friend of mine asked me a while ago to replicate the "tactik" game, and this a a perfect opportunity to test the CNC.

## FreeCAD modeling

![freecad model](../images/tactik/tactik_web.jpg)

<div class="sketchfab-embed-wrapper"> <iframe title="TacTik Game" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/1c655f5e0a584a79a92ac85465f2ce26/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/tactik-game-1c655f5e0a584a79a92ac85465f2ce26?utm_medium=embed&utm_campaign=share-popup&utm_content=1c655f5e0a584a79a92ac85465f2ce26" target="_blank" style="font-weight: bold; color: #1CAAD9;"> TacTik Game </a> by <a href="https://sketchfab.com/ejoz?utm_medium=embed&utm_campaign=share-popup&utm_content=1c655f5e0a584a79a92ac85465f2ce26" target="_blank" style="font-weight: bold; color: #1CAAD9;"> ejoz </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=1c655f5e0a584a79a92ac85465f2ce26" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

## Preparation for cutting

!!! Tips
    Use the TechDraw workbench on FreeCAD to obtain the *top view* on a drawing. I use the `A0 - blank template` to make sure I have enough space for my pieces. When I export my file in .svg format and open it on Inkscape, I make sure my dimensions are respected before cutting (using the ruler).

![](../images/tactik/tactiksvg_web.jpg)

Florian told me to convert my files into .dxf or .pdf format, and to just take one portion of the board game to test it.


## Files

- [FreeCAD file](../files/TacTikGame.FCStd)
- [Inkscape file](../files/Tactik.svg)