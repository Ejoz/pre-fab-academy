---
title: Watercolor Easel
---

# Watercolor Easel

![Watercolor Easel](../images/easel/watercolor_easel_web.jpg)

## What will it do ?
I wanted to build a portable easel for a while, and I found [a model online](https://stablo.fr/fr/fabricationartisanale) that suited my needs for outside painting. I thought it would be a great FreeCAD exercise, and here is the result.

## 3D Modeling
You can watch the full process on x4 speed below.
<iframe src="https://www.youtube.com/embed/9g8Xh5UWcy0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Model sharing
I learned how to export a FreeCAD model directly into Sketchfab, which is really a great way to share 3D models. I will look for an open source alternative if I can find one.
<div class="sketchfab-embed-wrapper"> <iframe title="Watercolor_Easel" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/c6f639edf61549278fd02d5df4dd6bf7/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/watercolor-easel-c6f639edf61549278fd02d5df4dd6bf7?utm_medium=embed&utm_campaign=share-popup&utm_content=c6f639edf61549278fd02d5df4dd6bf7" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Watercolor_Easel </a> by <a href="https://sketchfab.com/ejoz?utm_medium=embed&utm_campaign=share-popup&utm_content=c6f639edf61549278fd02d5df4dd6bf7" target="_blank" style="font-weight: bold; color: #1CAAD9;"> ejoz </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=c6f639edf61549278fd02d5df4dd6bf7" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

## Laser cutting preparation
Now that we have our model on FreeCAD, it's time to transfer our project into the real world. I have some Okoume planks at home and can use the laser cutter machine of the fablab to execute my files.

We need to flatten all of our 3D pieces. 

I think it is possible in FreeCAD directly, thanks to the [Laser Cut Interlocking addon]():

- Select all bodies in your model
![Select all bodies in your model](../images/easel/selectall_web.jpg)

- Export
![Export](../images/easel/2export_web.jpg)

- Hide the 3D view
![Hide the 3D view](../images/easel/2dview_web.jpg)

!!! fail
    I tried to export the files to .svg but I keep having an error message (```Export of this object type is not supported by drawing module```)

- So I'm using the TechDraw Module to export my 3d model in a compatible file (.svg):
![TechDraw](../images/easel/techdraw_web.jpg)

- It worked! I then import my .svg file in Inkscape and check that all dimensions are respected. It's now ready for the machine :smile:
![Inkscape](../images/easel/inkscape_web.jpg)

## Files
- [FreeCAD file](../files/Watercolor_Easel.FCStd)
- [Inkscape file](../files/Laser_Watercolor_Easel.svg)

## Laser cutting
I managed to finally book the laser cutting machine for myself an all afternoon to test my material and cut my design. I started with some cutting tests, and I choose to cut with theses parameters: `Speed: 20 % Power: 100% Frequency: 100%`

- Cutting process: ![](../images/easel/PXL_20211125_165226104.TS_exported_24802_1637940514615_web.jpg)
- The final pieces: ![](../images/easel/easel_PXL_20211126_075713647_web.jpg)
- The middle piece with magnets (to hold the watercolors): ![](../images/easel/easel_PXL_20211126_075330634_web.jpg)
- PVA glue process: ![](../images/easel/easel_PXL_20211126_084427887_web.jpg) ![](../images/easel/easel_original_a3bfb95d-268d-46e7-962e-83052f7ddc19_PXL_20211126_092216532_web.jpg)
- Final result: ![](../images/easel/easel_PXL_20211126_075423345_web.jpg)

## Hero shots
![](../images/easel/heroshot_easel_PXL_20211126_110346633.MOTION_web.jpg)
![](../images/easel/heroshot_easel_PXL_20211126_110530867.MOTION-01.COVER_web.jpg)