---
title: Electricity Basics
---

Jonah and Romain offered to give me a few courses in electronics before starting the Fab Academy in January.

# Ressources
- [Getting started in Electronics](http://www.zpag.net/Electroniques/Kit/Getting_Started_in_Electronics_-_3ed_-_[Forrest_M.Mims].pdf)
- [What is electricity?](https://learn.sparkfun.com/tutorials/what-is-electricity)
- [What is a circuit?](https://learn.sparkfun.com/tutorials/what-is-a-circuit)

# What is electricty ?

- Electrons = negative
- Protons = positive
- Neutrons = neutral

Electrons hates each other, so concentration = tension (voltage). High tension = lots of electrons. Low tension = not so much

Battery = two chambers, one with lots of E, one with none. When the holes leaves and there is an equality in the chambers, the battery died, ther is no attraction anymore

Voltage = difference of potential between two points of a circuit.

On periodic table, the higher the number, more conductive is the material (and less isolative (?) of course).

Resistor = country path / wire = highway

Current = amperes

Less resistance = more current

*V = I * R*

# Choose a resistor for a led
5V is too much for a 2V led.
Resistor value = tension de la source - tension requise par composant / courant suscité par composant