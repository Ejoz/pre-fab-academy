---
title: Example circuits
---

Jonah offered to prepare some assignements to help me prepare. Here are some examples of these.

## Step by step motor controler L293D

### Circuit
![Circuit](../images/electronics/stepper_circuit_web.jpg)

I had to recreate this circuit with a breadboard and an Arduino UNO. The goal is to activate the stepper motor (just make it work!)

First I connected the L239D to the breadboard and added every ground and power wires. I tried to keep things neat, but it becomes messy quickly. I then connected every other wires (stepper motor, arduino UNO and power bank.)

![Wires mess](../images/electronics/stepper_wires_web.jpg)

### Code
I then connected the Arduino to my computer and televersed a test code I found on the [Stepper library reference.](https://www.arduino.cc/en/Reference/Stepper)

```arduino
#include <Stepper.h>

const int stepsPerRevolution = 200;

Stepper myStepper(stepsPerRevolution, 9, 10, 11, 12);

int stepCount = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  myStepper.step(1);
  Serial.print("steps:");
  Serial.println(stepCount);
  stepCount++;
  delay(50);
}
```

You can observe the result on the video below, with a speed change. It worked and put a big smile on my face!
![type:video](../images/electronics/stepper_test.mp4)


### Voltage test
| Voltage | Works |
| ------- | ----- |
| 12V     | yes   |
| 9V      | yes   |
| 7V      | no    |

## Make the L293D

### Material needed
| Material name | Model   | Symbol |
| ------------- | ------- | ------ |
| DC Motor      |         |        |
| Diode         | D       |        |
| NPN           | PN2222A |        |
| PNP           | PN2907A |        |
| Resistor      | 1K Ohm  |        |

### Circuit
Much more difficult than the other one. It will do the same, but this time we make it ourselves.
![H-bridge](../images/electronics/h-bridge-circuit_web.jpg)

### Code
```arduino
void setup() {
  Serial.begin(9600);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT); 
}

void loop() {
  Serial.println("One way");
  digitalWrite(8, LOW);
  digitalWrite(9, HIGH);
  digitalWrite(10, HIGH);
  digitalWrite(11, LOW);
  delay(5000); 
  Serial.println("The other way");  
  digitalWrite(8, HIGH);
  digitalWrite(9, LOW);
  digitalWrite(10, LOW);
  digitalWrite(11, HIGH);
  delay(5000);
}
```

!!! fail
    This doesn't work. That let me test some debugging methods that Jonah showed me. You can use a multimeter to check if the current runs through the components. The key is to isolate the error.

We finally figured it out. We assumed that the transistor 1 was connected to the transistor 4, and it was in reality connected to the transistor 2. What a waste of time. But as Jonah said, "if we document it it was not a complete waste of our morning".

To debug, we tried to start over and put one half of the bridge. We put leds to test our assumptions, and finally figured it out.

- ![debug-circuit](../images/electronics/hbridge-debug_web.jpg)
- ![debug-circuit-leds](../images/electronics/hbridge-debug-leds_web.jpg)


The new code looks like this, I simplified based on our experiments
```arduino
void setup() {
  Serial.begin(9600);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop() {
  Serial.println("One way");
  digitalWrite(10, HIGH);
  digitalWrite(11, LOW);
  delay(10000); 
  Serial.println("The other way");  
  digitalWrite(10, LOW);
  digitalWrite(11, HIGH);
  delay(10000);
}
```