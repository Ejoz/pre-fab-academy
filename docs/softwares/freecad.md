---
title: FreeCAD
---

## Download FreeCAD
I use the RealThunder branch that you can download [here](https://github.com/realthunder/FreeCAD_assembly3/releases).

## Learning FreeCAD
These are some of the bests tutorials I found to learn FreeCAD. Of course the documentation itself is quite complete, but nothing is better than a tutorial.

- [Maker Tales - Complete introduction for beginners](https://www.youtube.com/playlist?list=PL6Fiih6ItYsWCE20KtUJYpiDPrCA2rVpN)
- [Barbatronic - Personalize FreeCAD](https://www.youtube.com/watch?v=BXCA3jidTrM&t=1004s)
- [OficineRobotica - Create for 3D printing](https://www.youtube.com/watch?v=OWNrYvxpG4k)

## Some tips
- [Write a text on a surface](https://www.youtube.com/watch?v=aVZmgUbAl-E)
- Display your object: [`View` > `Turntable`](https://wiki.freecadweb.org/Std_DemoMode)
- Get models of electronic cards on [GrabCAD](https://grabcad.com/). .STEP files are directly importables in FreeCAD.
- Rotate around a sketch with `Alt` + `Left click`