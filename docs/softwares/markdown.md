---
title: Markdown
---

# Markdown Cheat Sheet
- [The Markdown Guide](https://www.markdownguide.org)
All the basic syntax can be found on the Markdown guide. I'll keep on this page the syntax I forgot the most to quickly find them when I need it.

## Extended Syntax
These elements extend the basic syntax by adding additional features. Not all Markdown applications support these elements.

### Table
If you want to convert a previous table into markdown, here is a handful [tool](https://tabletomarkdown.com/).
```
| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |
```

### Footnote
```
Here's a sentence with a footnote. [^2]

[^2]: This is the footnote.
```
Here's a sentence with a footnote. [^2]

[^2]: This is the footnote.

### Strikethrough
```
~~The world is flat.~~
```
~~The world is flat.~~

## [Admonitions](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)
```
markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.superfences
```


### Simple
```
!!! note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

!!! note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

### Collapsible
```
??? note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```
??? note

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

### Type qualifiers
- `note`
!!! note

- `abstract`, `summary`, `tldr`
!!! abstract

- `info`, `todo`
!!! info

- `tip`, `hint`, `important`
!!! tip

- `success`, `check`, `done`
!!! success

- `question`, `help`, `faq`
!!! question

- `warning`, `caution`, `attention`
!!! warning

- `failure`, `fail`, mis`sing
!!! fail

- `danger`, `error`
!!! danger

- `bug`
!!! bug

- `example`
!!! example

- `quote`, `cite`
!!! quote


### Inline

**Must be placed before the text/image/code.**

!!! info inline

    Lorem ipsum dolor sit amet, consectetur
    adipiscing elit. Nulla et euismod nulla.
    Curabitur feugiat, tortor non consequat
    finibus, justo purus auctor massa, nec
    semper lorem quam in massa.

```
!!! info inline

    Lorem ipsum dolor sit amet, consectetur
    adipiscing elit. Nulla et euismod nulla.
    Curabitur feugiat, tortor non consequat
    finibus, justo purus auctor massa, nec
    semper lorem quam in massa.
```
<br> 

### Inline end
!!! info inline end

    Lorem ipsum dolor sit amet, consectetur
    adipiscing elit. Nulla et euismod nulla.
    Curabitur feugiat, tortor non consequat
    finibus, justo purus auctor massa, nec
    semper lorem quam in massa.

```
!!! info inline end

    Lorem ipsum dolor sit amet, consectetur
    adipiscing elit. Nulla et euismod nulla.
    Curabitur feugiat, tortor non consequat
    finibus, justo purus auctor massa, nec
    semper lorem quam in massa.
```






