---
title: Image and Video Optimization
---

## Tips
- For web, better use 1280*720

## Image Magick
[Image Magick](https://imagemagick.org/index.php) is a handy software that deals with digital images. I used it when I was working on computer vision models and I think it will be useful for the Fab Academy too. I am just beginning my journey into a vast and weighty documentation, and I need to be aware of the digital pollution I create.

My objective is to have some practical command lines at hand to resize my images.

### Install Image Magick
```
sudo apt-get install imagemagick
```

### Resize every images (.jpg and .png) in a folder
```
mogrify -resize 256x256 *.jpg *.png
```

## gThumb
User interface and possibility to deal with multiple files.

## FFmpeg
Command line tool. Can convert videos very easily.
```
sudo apt-get install ffmpef
```

### Video compression
```
ffmpeg -i inputfile.mov -vf scale=1280x720 outputfile.mp4
```

### Video cutting
```
ffmpeg -i inputfile.mov -ss 00:00:13 -to 00:00:23 -vf scale=1280x720 outputfile.mp4 
```

## Kris video
<iframe width="560" height="315" src="https://www.youtube.com/embed/Q1xU3PMvlk0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Useful links
- [Image Magick: resize or scaling](https://legacy.imagemagick.org/Usage/resize/)
- [Ubuntu documentation about Image Magick](https://doc.ubuntu-fr.org/imagemagick)
- [gThumb](https://doc.ubuntu-fr.org/gthumb)
- [FFmpeg](http://www.ffmpeg.org/)
