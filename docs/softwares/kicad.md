---
title: KiCAD
---

# KiCAD: Open Source Software and Fab Library

Last week the Aalto Fablab visited us and I met Kris, who is the Fab Academy Instructor of Aalto. He made a serie of videos on YouTube during the pandemic to help his students understand some major concepts of the courses. I followed his tutorial on KiCAD and it helped me a lot to jump into this software that scared me before.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q-yT332uCWY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I think I will save on this website a lot of his videos as I go on.