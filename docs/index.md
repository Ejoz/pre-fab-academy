---
title: Home
hide:
  - navigation
---

# A journey to Fab Academy

Three months to go before the Fab Academy beginning. I'm excited to learn all these new things, and I decided to spend a lot of my time in the lab.

I will be attending Fab Academy in the [Fab Lab Sorbonne](https://fablab.sorbonne-universite.fr/wiki/doku.php).

- [Personnal Planning](https://docs.google.com/spreadsheets/d/1BUXTF1j47x7TRqfL-Yg9-JVE7FH4scgyUPdfGN1KETo/edit#gid=1451347155)

