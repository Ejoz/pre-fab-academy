---
title: 3D Printer
---
# Ender 3 V2
I am lucky enough to have a 3D printer at home. We bought it during last year pandemic to keep us busy, and we loved it. I first experimented with 3D printing as a librarian, when I was organizing a "near fablab experience" for our readers. My approach is now "maker-oriented", and I will keep track on this page of the best calibration parameters for our machine.

## Specifications
- Model: Creality Ender 3 V2
- Plate: 220x220x250mm
- Filament: PLA/TPU/PETG

![](../images/ender3/PXL_20211126_164404441_web.jpg)

## PLA
| Reference    | Filament type | Color | Best temp | Comments                              |
| ------------ | ------------- | ----- | --------- | ------------------------------------- |
| Enotepad     | PLA           | Green |           | Never had problems with that filament |
| Ender Series | PLA           | White |           | Adhesion problems                     |
| Enotepad     | PLA           | Grey  |           |                                       |
| ?            | PLA           | Wood  | 185       ||

## Temperature tower
![](../images/ender3/PXL_20211126_152450454_web.jpg)

When I receive my first wood filament, I wanted to do it right and test the correct settings. I learned how to make a temperature tower by [modifying the Gcode](https://www.lesimprimantes3d.fr/forum/topic/2481-tuto-cr%C3%A9er-une-tour-pour-d%C3%A9finir-la-temp%C3%A9rature-dextrusion-id%C3%A9ale/). Here are the different steps to create yours.

1. Download (or create) a .stl file of a tower. I used [one found on Cults](https://cults3d.com/fr/mod%C3%A8le-3d/outil/joebob-customized-temperature-tower-version-2), but any model will do.
2. Slice your model on Cura and generate the Gcode. For every layer when you want a temperature change, you need to insert lines on the Gcode to change the temperature. Lines beginning by `;` are comments. `M104` means that you define a new temperature, `M105` is a report. `M109` indicates to your machine to wait for the temperature to be the one you indicated.
```gcode
;TowerTemp set to 185
M104 S185
M105
M109 S185
```
You need to insert these lines every time you want to change the temperature of the extrusion.
!!! tips
    You can find the right layer by typing `Ctrl+F` on your text editor and searching for `;LAYER:LayerNumber`.

![](../images/ender3/slicertowertemp_web.jpg)

## The ultimate test

Learn more on this test on [3D Benchy website](https://www.3dbenchy.com/). It is really well explained and the best test I ever used to check the quality of my settings.

![](../images/ender3/PXL_20211126_165703145_web.jpg)

## Links
- [Gcode (Marlin Software)](https://marlinfw.org/meta/gcode/)