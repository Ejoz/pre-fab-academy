---
title: CNC Shop Bot
---

Florian kindly teached us (Ninon and I) to use the CNC Shop Bot that we have in the machines room. Here are some of the detailed process below.

# CNC Shop Bot

![](../images/shopbot/shopbot_PXL_20211118_141721907_web.jpg)

Jonah made some slides about our CNC when he was a FabAcademy student. Thank you for that! I will make a complete example later on. I think the most important thing to understand is how to chosse your tool and speed.

<iframe src="//www.slideshare.net/slideshow/embed_code/key/gw7gx8k95KP9gJ" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/JonahRossMarrs/shop-bot-training" title="Shop bot training" target="_blank">Shop bot training</a> </strong> from <strong><a href="https://www.slideshare.net/JonahRossMarrs" target="_blank">Jonah Marrs</a></strong> </div>


## Some pictures
![](../images/shopbot/shopbot_PXL_20211118_132338374_web.jpg)
![](../images/shopbot/shopbot_PXL_20211118_144430175_web.jpg)