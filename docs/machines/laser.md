---
title: Epilog Fusion M2
---

## How to work with the machine
The Fablab owns a [Epilog Fusion M2 40](https://facilities.create.aau.dk/wp-content/uploads/Epilog_Guide_Raster-1.pdf). A previous student described the steps for using this machine, as you can read below.

1. Turn ON the machine. The cutting board lowers automatically.
2. Position the material to be cut inside the machine, at the desired location, and place some weights to keep it flat and prevent it from moving. Note : The origin (0,0) of the machine is set on the top left hand corner by default.
3. Adjust the height of the cutting board using the appropriate tool (height is not the same for CO2 and fiber lasers).
4. Adjust the machine origin point by moving the optical laser above the material to be cut, according to the 2D drawing to be cut.
5. Prepare your cutting job in CorelDRAW (paid SW):
6. Position objects correctly, adjust size if needed, select “hairline” for the stroke line type when using vector image, etc.
7. Click on Print (or shortcut CTRL+P), and select the printer preferences.
8. Set the type of image to be printed : Raster or Vector (if raster, select the desired DPI resolution).
9. Set the cut parameters (speed / power / frequency).
10. Click “Apply” and “Print”. => At this stage, the job should be available for start on the laser cutter interface.
11. Make sure that ventilation systems are ON.
12. Make sure that the machine door is closed and start the job.
13. Watch out for the whole cutting operation.

## Tests
I want to do some tests before cutting my [project](../projects/easel.md)

!!! Warning
    The first time I used the laser cutter, I had to clean the lense because it was unabled to cut clearly on straight lines. Circles were fine though.

| CUT (VECTOR)     |       |       |           |                                      |
| ---------------- | ----- | ----- | --------- | ------------------------------------ |
|                  | Speed | Power | Frequency | Comments                             |
| CP Okoumé 3mm    | 40    | 100   | 50        | 2x circle, 4x square                 |
|                  | 30    | 100   | 50        | 1x circle, 4x square                 |
|                  | 20    | 100   | 100       | 2x OK                                |
|                  | 15    | 100   | 100       | 2x OK but too cramé                  |
|Cardboard 5,5mm   | 30	   | 40	   | 50	       | 3x
|                  |       |       |           |                                      |
| ENGRAVE (RASTER) |       |       |           |                                      |
| CP Okoumé 3mm    | 100   | 100   |           | 1x, résultats très nets jusqu'à 12pt |

Les tests en action:

![](../images/laser-cutter/PXL_20211214_100123985_web.jpg)

Les détails sont vraiment précis, je pense que je pourrais pousser encore plus loin et produire du texte plus petit. La prochaine étape est de combiner *vector* + *raster*.

![](../images/laser-cutter/PXL_20211214_103747016_web.jpg)

